#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "hash_index.h"



#define   SETCOLOR_GREEN   "\033[1;32m"
#define   SETCOLOR_RED     "\033[1;31m"
#define   SETCOLOR_NORMAL    "\033[0;39m"


void print_green(char c)
{
	
	printf("%s%c%s", SETCOLOR_GREEN, c, SETCOLOR_NORMAL);
}



static char *get_file(char *keyword)
{
	static char hash_file[512] = HASH_DIR;
		
	sprintf(hash_file + HASH_DIR_LEN, "%02x/", do_hash(keyword));
	strcat(hash_file, keyword);
	return hash_file;
}
		
static void print_3_line(char *data, char *word, uint32_t file_pos)
{
	char buf[1024];
	char *p = data + file_pos;
	char *q = buf;
	int on = 2;

	while (on) {
		if (*p == '\n')
			on--;
		if (p <= data)
			break;
		p--;
	}
	p += 2;
	on = 3;
	while (on) {
		if (*p == '\n')
			on--;
		*q++ = *p++;
	}
	*q = '\0';

	q = buf;
	while (*p) {
		p = strstr(q, word);
		if (!p) {
			printf("%s", q);
			break;
		}
		for (*q; q < p; q++) 
			printf("%c", *q);
		p += strlen(word);
		for (*q; q < p; q++)
			print_green(*q);
	}
}

/*
 * Query the keywords!
 * 
 * count:    is the keywords count.
 * keywords: The address of the keywords strings.
 */
static void query(int count, char *keywords[])
{
	char buf[0x100000];
	char *key;
	char *hash_file;
	int i;
	int fd1, fd2;
	struct key_words kw;
	
	for (i = 0; i < count; i++) {
		key = keywords[i];
		hash_file = get_file(key);
		fd1 = open(hash_file, O_RDONLY);
		if (fd1 < 0) 
			continue;
		while(read(fd1, &kw, sizeof kw) > 0) {
			fd2 = open(kw.file, O_RDONLY);
			if (fd2 < 0) {
				printf("Open file %s error!\n", kw.file);
				continue;
			}
			read(fd2, buf, sizeof buf);
			print_3_line(buf, key, kw.file_pos);
			printf("FROM file: %s\n\n", kw.file);
			close(fd2);
		}
		close(fd1);
	}
}
			
			
		
	

static void usage(void)
{
	printf("USAGE: query keywords ....\n");
}


int main(int argc, char *argv[])
{
	if (argc < 2) {
		usage();
		exit(0);
	}

	query(argc - 1, &argv[1]);
		
	return 0;
}
