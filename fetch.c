#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>

#include "hash_index.h"


static inline int is_ss_char(char c)
{
	if ((c >= 'A' && c <= 'Z') ||
	    (c >= 'a' && c <= 'z') ||
	    (c >= '0' && c <= '9'))	    
		return 1;
	else
		return 0;
}

static inline int is_space(char c)
{
	return c == ' ';
}

static char word[128];
/*
 * Get a word from _p_, skip the none-ss-char first, 
 * then returen the begining of the word in _word_,
 * and last, return the begining of the next word.
 */
static char *get_word(char *str)
{
	char *p;
	
	while (*str && !is_ss_char(*str)) 
		str++;
	if (*str == 0) 
		return NULL;
	
	p = word;
	while (is_ss_char(*str) || *str == '.') 
		*p++ = *str++;
	*p = 0;	
	
	return str;
}

static void tolower_str(char *str)
{
	while (*str) {
		if (*str >= 'A' && *str <= 'Z')
			*str = *str + 0x20;
		str++;
	}
}

/*
 * Do the boring but Cool thing
 */
static void parse_word(void)
{
	int len = strlen(word);
	char *p = word + len - 1;
	
        /* Drop the last period mark */
	while (*p == '.')
		*p-- = '\0';

	tolower_str(word);
	
	
	/* 'ies' and 'ied' to 'y' */
	if (!strncmp(p - 2, "ies", 3) ||
	    !strncmp(p - 2, "ied", 3)) {
		*(p - 2) = 'y';
		*(p - 1) = '\0';
		return;
	}

	/*
	 * Drop the trailing 'e', making keywords like 'waiting' and 'waite' 
	 * are treated same.
	 */
	if (*p == 'e') {
		*p = '\0';
		return;
	}
		
	/* 
	 * treat the word with 's' ending as plural, 
	 *  even it may be not true 
	 */
	if (*p == 's')
		*p--  = '\0';
	
	/* Drop 'ing' */
	if (!strncmp(p - 2, "ing", 3)) {
		*(p - 2) = '\0';
		return;
	}
	
	/* Drop 'er' */
	if (!strncmp(p - 1, "er", 2)) {
		*(p - 1) = '\0';
		return;
	}
		
	/* Drop 'ed' */
	if (!strncmp(p - 1, "ed", 2)) {
		*(p - 1) = '\0';
		return;
	}
}

	


static void install_word(char *dir, char *word, 
			 const char *file, uint32_t file_pos)
{
	int fd;
	uint32_t flag = 0;
	char hash_file[512];
	struct key_words key;
	
	strcpy(hash_file, dir);
	strcat(hash_file, "/");
	strcat(hash_file, word);

	if (access(hash_file, F_OK) < 0)	
		flag = O_CREAT;
	fd = open(hash_file, O_WRONLY | O_APPEND | flag , 0644);
	if (fd < 0)
		printf("ERROR: open hash file %s error!\n", hash_file);

	memset(&key, 0, sizeof key);
	key.file_pos = file_pos;
	strcpy(key.file, file);
	if ((write(fd, &key, sizeof key)) < 0)
		printf("EROOR: write hash file %s error!\n", hash_file);
	close(fd);
}

static void hash_word(char *word, const char *file, uint32_t file_pos)
{
	int hash;
	char hash_dir[HASH_DIR_LEN + 3]= HASH_DIR;
	
	hash = do_hash(word);
	
	sprintf(hash_dir + HASH_DIR_LEN, "%02x", hash);
	if (access(hash_dir, F_OK) < 0) {
		if (mkdir(hash_dir, 0700) < 0) {
			printf("mkdir: %s error!\n", hash_dir);
			return;
		}
	}
	install_word(hash_dir, word, file, file_pos);
}

static void do_parse(char *buf, const char *file, uint32_t file_pos)
{
	char *p = buf;
		
	while (1) {
		p = get_word(p);
		if (!p) 
			break;
		parse_word();
		if (*word <= ' ')
			continue;
		hash_word(word, file, file_pos + p - buf - strlen(word));
	}
}
	

static void parse_file(const char *file)
{
	int fd;
	uint32_t file_pos = 0;
	char buf[0x100000];

	fd = open(file, O_RDONLY);
	if (fd < 0) {
		printf("Open file _%s_ error!\n");
		return;
	}

	while((read(fd, buf, sizeof buf)) > 0) {
		do_parse(buf, file, file_pos);
		file_pos += sizeof buf;
	}
	
	close(fd);
}


void fetch()
{
	DIR *dir;
	struct dirent *de;
	
	dir = opendir("./");
	if (dir < 0) {
		printf("open current directory error!\n");
		return;
	}

	while((de = readdir(dir))) {
		/* 
		 * we do nothing to these files started '.', 
		 *   "." and ".." included. 
		 */
		if (!strcmp(de->d_name, "a.out") ||
		    *de->d_name == '.')			
			continue;
#if 1
		printf("Parsing file %s\n", de->d_name);
#endif
		parse_file(de->d_name);
	}
}
	
