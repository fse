all: fetch query

fetch:
	gcc -g main.c fetch.c -o fetch
query:
	gcc -g query.c -o query

clean:
	rm -f *~ a.out fetch query
