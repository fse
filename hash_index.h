#ifndef HASH_INDEX_H
#define HASH_INDEX_H

#include <stdint.h>

struct key_words {
	uint32_t file_pos;
	char file[256];
};


#define HASH_DIR         ".hash_data/"
#define HASH_DIR_LEN     (strlen(HASH_DIR))

/* 
 * The hash function
 */
static int do_hash(char *word)
{
	uint32_t val = 1;

	while (*word)
		val += (val >> 8) + 3 + *word++;

	return val & 0xff;
}


#endif /* hash_index.h */
