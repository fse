#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "hash_index.h"

extern void fetch(void);

static void init()
{
	if (mkdir(HASH_DIR, 0700) < 0) {
		printf("ERROR: init hash dir error!\n");
		return;
	}
}


int main(void)
{
	init();
	
	fetch();
	return 0;
}
