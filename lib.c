#include <stdio.h>


#define   SETCOLOR_GREEN   "\033[1;32m"
#define   SETCOLOR_RED     "\033[1;31m"
#define   SETCOLOR_NORMAL    "\033[0;39m"


void print_green(char *str)
{
	
	printf("%s%s%s", str, SETCOLOR_SUCCESS, str, SETCOLOR_NORMAL);
}
